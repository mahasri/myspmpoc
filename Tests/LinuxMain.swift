import XCTest

import MySPMPocTests

var tests = [XCTestCaseEntry]()
tests += MySPMPocTests.allTests()
XCTMain(tests)
